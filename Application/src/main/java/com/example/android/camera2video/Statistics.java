package com.example.android.camera2video;

import android.util.Log;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Statistics {

    private static final String TAG="Statistics";
    private static final double beta=0.9;

    private static Statistics single_instance = null;

    // private constructor restricted to this class itself
    private Statistics()
    {
        mInterFrame=0;
        mFPS=0;
        mCodecFrame=0;
        mCaptureFrame=0;
        mTimestamps=new LinkedBlockingQueue<>();
        mLasts=0;
    }


    // static method to create instance of Singleton class
    public static Statistics getInstance()
    {
        if (single_instance == null)
            single_instance = new Statistics();

        return single_instance;
    }
    /**
     * Codec stats
     */
    private long mInterFrame;
    private double mFPS;
    private double mLatency;
    private int mCodecFrame;
    private int mCaptureFrame;
    private Queue<Long> mTimestamps;
    private long mLasts;
    private int mFrameSkipped;

    public void computeEncoderStats(){
        long buf=System.currentTimeMillis();
        long interframe=buf-mInterFrame;
        double FPS=1000/interframe;
        mFPS=beta*mFPS+(1-beta)*FPS;
        mLatency=beta*mLatency+(1-beta)*(buf-mLasts);
        mInterFrame=buf;
        mCodecFrame++;
        Log.d(TAG,"Frame="+mCodecFrame+", FPS="+FPS+", FPSAvg="+(int)(mFPS)+", Latency="+(buf-mLasts)+", LatencyAvg="+(int)mLatency);

        //try{
        //    long buf2=mTimestamps.remove();
        //    mCodecFrame++;
        //    Log.d(TAG,"Frame="+mCodecFrame+", FPS="+FPS+", FPSAvg="+mFPS+", Latency="+(buf-buf2));
        //}catch(Exception e){
        //    mFrameSkipped++;
        //    Log.d(TAG,"Error in TS");
        //    e.printStackTrace();
        //}
        mCodecFrame++;

    }


    public void pushTs(long timestamp){
        mCaptureFrame++;
        this.mLasts = System.currentTimeMillis();
        //mTimestamps.add(timestamp);
    }

    public void flushTsQueue(){
        Long response;
        do {
            response=mTimestamps.poll();
        }while(response!=null);
    }
}
