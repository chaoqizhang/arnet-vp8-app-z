package com.example.android.camera2video;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecList;
import android.os.Build;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static android.media.MediaFormat.KEY_CAPTURE_RATE;

public class VideoEncoding {

    private static final String TAG="VideoEncoding";

    /**
     * MediaCodec
     */
    private MediaCodec mMediaCodec;
    private Surface mEncoderSurface;
    private MediaCodec.BufferInfo mBufferInfo;

    /**
     * Codec parameters
     */
//    private static final String MIME_TYPE = "video/avc"; // H.264 AVC encoding
    private static final String MIME_TYPE = "video/x-vnd.on2.vp8"; // H.264 AVC encoding
//    private static final String VPX_DECODER_NAME = "OMX.google.vpx.decoder";
//    private static final String VPX_ENCODER_NAME = "OMX.qcom.video.encoder.vp8";
//    private static final String VPX_ENCODER_NAME = "OMX.qcom.video.encoder.avc";

    private static final int FRAME_RATE = 30; // 30fps
    private static final float IFRAME_INTERVAL = (float) 1.0; // 1 seconds between I-frames
    // Sync object to protect stream state access from multiple threads.

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;
    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;
    private UDPTransmission mUDPTrans;

    //public void setmBackgroundHandler(Handler h){mBackgroundHandler=h;}

    private static final String VP8_MIME_TYPE = "video/x-vnd.on2.vp8";
    // List of supported HW VP8 codecs.
    private static final String[] supportedHwCodecPrefixes =
            {"OMX.qcom.", "OMX.Nvidia." };
    private static final int[] supportedColorList = {
            CodecCapabilities.COLOR_FormatSurface,
            CodecCapabilities.COLOR_FormatYUV420Flexible
    };
    // Helper struct for findVp8HwEncoder() below.
    private static class EncoderProperties {
        EncoderProperties(String codecName, int colorFormat) {
            this.codecName = codecName;
            this.colorFormat = colorFormat;
        }
        public final String codecName; // OpenMax component name for VP8 codec.
        public final int colorFormat;  // Color format supported by codec.
    }
    private static EncoderProperties findVp8HwEncoder() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
            return null; // MediaCodec.setParameters is missing.

        for (int i = 0; i < MediaCodecList.getCodecCount(); ++i) {
            MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);
            if (!info.isEncoder()) {
                continue;
            }
            String name = null;
            for (String mimeType : info.getSupportedTypes()) {
                if (mimeType.equals(VP8_MIME_TYPE)) {
                    name = info.getName();
                    break;
                }
            }
            if (name == null) {
                continue;  // No VP8 support in this codec; try the next one.
            }
            Log.d(TAG, "findVp8HwEncoder: Found candidate encoder " + name);
            CodecCapabilities capabilities =
                    info.getCapabilitiesForType(VP8_MIME_TYPE);
            for (int colorFormat : capabilities.colorFormats) {
                Log.d(TAG, "findVp8HwEncoder:   Color: 0x" + Integer.toHexString(colorFormat));
            }

            // Check if this is supported HW encoder
            for (String hwCodecPrefix : supportedHwCodecPrefixes) {
                if (!name.startsWith(hwCodecPrefix)) {
                    continue;
                }
                // Check if codec supports either yuv420 or nv12
                for (int supportedColorFormat : supportedColorList) {
                    for (int codecColorFormat : capabilities.colorFormats) {
                        if (codecColorFormat == supportedColorFormat) {
                            // Found supported HW VP8 encoder
                            Log.d(TAG, "findVp8HwEncoder: Found target encoder " + name +
                                    ". Color: 0x" + Integer.toHexString(codecColorFormat));
//                            Log.d(TAG, "findVP8HwEncoder: *** " + properties.codecName);
                            return new EncoderProperties(name, codecColorFormat);
                        }
                    }
                }
            }
        }
        return null;  // No HW VP8 encoder.
    }

    public void setUpMediaCodec(UDPTransmission UDPTrans) throws IOException {
        startBackgroundThread();
        mUDPTrans = UDPTrans;

        if(ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN) {
            Log.d(TAG, "Big Endian ");
        } else {
            Log.d(TAG, "Little Endian ");
        }

//        EncoderProperties properties = findVp8HwEncoder();
//        if (properties == null) {
//            throw new RuntimeException("Can not find HW VP8 encoder");
//        }
//        Log.d(TAG, "findVP8HwEncoder: *** " + properties.codecName);
        //MediaFormat format =
        //        MediaFormat.createVideoFormat(MIME_TYPE,
        //                1024, 768);
        MediaFormat format =
                MediaFormat.createVideoFormat(MIME_TYPE,
                        320, 240);
        /**
         * Set encoding properties. Failing to specify some of these can cause
         * the MediaCodec configure() call to throw an exception.
         */
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
//                MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible);
//        format.setInteger(MediaFormat.KEY_COLOR_FORMAT, properties.colorFormat);

        format.setInteger(MediaFormat.KEY_BIT_RATE, 125000);
        format.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
        format.setInteger(MediaFormat.KEY_LATENCY, 0);
        format.setInteger(MediaFormat.KEY_PROFILE,
//                MediaCodecInfo.CodecProfileLevel.AVCProfileConstrainedBaseline);
                MediaCodecInfo.CodecProfileLevel.VP8ProfileMain);
//                MediaCodecInfo.CodecProfileLevel.VP8Level_Version0);
        format.setFloat(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL);
        format.setInteger(KEY_CAPTURE_RATE, FRAME_RATE);
        Log.i(TAG, "configure video encoding format: " + format);
        // Create/configure a MediaCodec encoder.
        mMediaCodec = MediaCodec.createEncoderByType(MIME_TYPE);
//        mMediaCodec = MediaCodec.createByCodecName(VPX_ENCODER_NAME);
//        mMediaCodec =  MediaCodec.createByCodecName(properties.codecName);

        mMediaCodec.setCallback(new VideoEncodingCallback(), mBackgroundHandler);
        mMediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        mEncoderSurface = mMediaCodec.createInputSurface();
        mMediaCodec.start();
    }

    public Surface getSurface(){
        return mEncoderSurface;
    }

    public void removeMediaCodec(){
        mUDPTrans.delete();
        mUDPTrans=null;
        if (null != mMediaCodec) {
            mMediaCodec.stop();
            mMediaCodec.release();
            mMediaCodec = null;
        }
        stopBackgroundThread();
    }

    class VideoEncodingCallback extends MediaCodec.Callback{
        @Override
        public void onInputBufferAvailable(@NonNull MediaCodec codec, int index) {
            Log.d("MediaCodec", "InputBuffer "+index+" "+System.currentTimeMillis());
        }

        public byte[] int2byteLe(int num) {
            return new byte[] {
                    (byte) (num & 0xFF),
                    (byte) ((num >> 8) & 0xFF),
                    (byte) ((num >> 16) & 0xFF),
                    (byte) ((num >> 24) & 0xFF),
            };
        }

        @Override
        public void onOutputBufferAvailable(@NonNull MediaCodec codec, int index,
                                            @NonNull MediaCodec.BufferInfo info) {
            Log.d("MediaCodec","OutputBuffer Type: "+info.flags);
            Statistics.getInstance().computeEncoderStats();
            //Log.d("MediaCodec", "Frame "+index+" "+System.currentTimeMillis());
            ByteBuffer outputBuffer = codec.getOutputBuffer(index);
            MediaFormat bufferFormat = codec.getOutputFormat(index);
            Log.d(TAG,"Format: "+bufferFormat.getString(MediaFormat.KEY_MIME));
            byte[] outData = new byte[info.size];
            outputBuffer.get(outData);

            int sizeOfFrame = info.size;
            long PTS = info.presentationTimeUs;
            int IVFFRAMEHEADERSIZE = 12;
            byte[] ivfFrame = new byte[IVFFRAMEHEADERSIZE+sizeOfFrame];
//            for(int i = 0; i < sizeOfFrame; i = i+1) {
//                ivfFrame[IVFFRAMEHEADERSIZE+i] = outData[i];
//            }
            System.arraycopy(outData, 0, ivfFrame, IVFFRAMEHEADERSIZE, sizeOfFrame);
            ivfFrame[0] = (byte) ((sizeOfFrame) & 0xFF);
            ivfFrame[1] = (byte) ((sizeOfFrame >> 8) & 0xFF);
            ivfFrame[2] = (byte) ((sizeOfFrame >> 16) & 0xFF);
            ivfFrame[3] = (byte) ((sizeOfFrame >> 24) & 0xFF);
            ivfFrame[4] = (byte) ((PTS) & 0xFF);
            ivfFrame[5] = (byte) ((PTS >> 8) & 0xFF);
            ivfFrame[6] = (byte) ((PTS >> 16) & 0xFF);
            ivfFrame[7] = (byte) ((PTS >> 24) & 0xFF);
            ivfFrame[8] = (byte) ((PTS >> 32) & 0xFF);
            ivfFrame[9] = (byte) ((PTS >> 40) & 0xFF);
            ivfFrame[10] = (byte) ((PTS >> 48) & 0xFF);
            ivfFrame[11] = (byte) ((PTS >> 56) & 0xFF);

            mUDPTrans.sendData(ivfFrame);
//            mUDPTrans.sendData(outData);
            Log.d(TAG, " sendData : " + outData.length);
            codec.releaseOutputBuffer(index,false);

        }

        @Override
        public void onError(@NonNull MediaCodec codec, @NonNull MediaCodec.CodecException e) {
            Log.d("MediaCodec", "Error");
            e.printStackTrace();
        }

        @Override
        public void onOutputFormatChanged(@NonNull MediaCodec codec, @NonNull MediaFormat format) {
            Log.d("MediaCodec", "Outputformatchanged");
            Log.d("MediaCodec", format.toString());
        }

    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());

    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
