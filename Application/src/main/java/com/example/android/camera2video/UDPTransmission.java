package com.example.android.camera2video;

import android.os.AsyncTask;
import android.provider.ContactsContract;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class UDPTransmission {

    InetAddress mServerIp;
    int mServerPort;
    DatagramSocket mDs;

    public UDPTransmission(String Ip, int port){
        try {
            mServerIp= InetAddress.getByName(Ip);
            mServerPort=port;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            mDs = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void sendData(byte[] bu){
        //RTPpacket rp=new RTPpacket(bu,bu.length);
        //byte[] buff = new byte[bu.length+200];
        //int len=rp.getpacket(buff);
        DatagramPacket dp = new DatagramPacket(bu, bu.length, mServerIp, mServerPort);
        try {
            mDs.send(dp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delete(){
        if(mDs!=null) {
            mDs.close();
        }
    }
    //private class SendTask extends AsyncTask<Void, Void, Void> {
    //    @Override
    //    protected Void doInBackground(byte[]... bytes) {
    //        String msg="packet "+System.currentTimeMillis();
    //        DatagramPacket dp = new DatagramPacket(msg.getBytes(), msg.length(), mServerIp, mServerPort);
    //        try {
    //            mDs.send(dp);
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        }
    //        return null;
    //    }
    //}

}
