package com.example.android.camera2video;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class ARLabelSurfaceView extends SurfaceView {
    private static final String TAG = "ARLabelSurfaceView";
    private SurfaceHolder mSurfaceHolder;
    private Thread thread;
    private boolean flag;
    private Canvas mCanvas;
    private Paint mPaint;


    public ARLabelSurfaceView(Context context) {
        super(context);
        Log.d(TAG, "constructor initialized");
        mSurfaceHolder = getHolder();
        mSurfaceHolder.setFormat(PixelFormat.TRANSPARENT);
        this.setZOrderOnTop(true);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.BLUE);
        mPaint.setStyle(Paint.Style.STROKE);

        //        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mCanvas = mSurfaceHolder.lockCanvas();
                if(null == mCanvas) {
                    Log.e(TAG, "canvas is null");
                } else {
                    mCanvas.drawRect(100,100,200,200,mPaint);
                    mSurfaceHolder.unlockCanvasAndPost(mCanvas);
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }



}
